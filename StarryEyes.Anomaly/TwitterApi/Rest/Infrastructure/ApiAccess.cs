﻿namespace StarryEyes.Anomaly.TwitterApi.Rest.Infrastructure
{
    public class ApiAccess
    {
        private readonly string _path;
        private readonly string _param;

        public ApiAccess(string path, string param = null)
        {
            this._path = path;
            this._param = param;
        }

        public override string ToString()
        {
            return (ApiAccessProperties.ApiEndpoint) +
                   (_path.StartsWith("/") ? _path.Substring(1) : _path) +
                   (string.IsNullOrEmpty(_param) ? "" : ('?' + _param));
        }

        public static implicit operator string(ApiAccess access)
        {
            // ReSharper disable RedundantToStringCall
            System.Diagnostics.Debug.WriteLine("API Access:" + access.ToString());
            // ReSharper restore RedundantToStringCall
            return access.ToString();
        }
    }
}