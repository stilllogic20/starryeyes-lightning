﻿using System;
using System.Net;
using System.Security.Cryptography;
using AsyncOAuth;

namespace StarryEyes.Anomaly
{
    public static class Core
    {
        internal const string DefaultConsumerKey = "3rJOl1ODzm9yZy63FACdg";

        internal const string DefaultConsumerSecret = "5jPoQ5kQvMJFDYRNE8bQ4rHuds4xJqhvgNJM4awaE8";

        public static void Initialize()
        {
            // init oauth util
            OAuthUtility.ComputeHash = (key, buffer) =>
            {
                using (var hmac = new HMACSHA1(key))
                {
                    return hmac.ComputeHash(buffer);
                }
            };
            UseSystemProxy = true;
        }

        public static bool UseSystemProxy { get; set; }

        public static Func<IWebProxy> ProxyProvider { get; set; }

        internal static IWebProxy GetWebProxy()
        {
            if (UseSystemProxy)
            {
                return WebRequest.GetSystemWebProxy();
            }
            return ProxyProvider == null ? null : ProxyProvider();
        }
    }
}
