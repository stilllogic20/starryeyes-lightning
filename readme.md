# Krile Lightning

Krile Lightning is a Twitter Client, contains performance optimizations and bug fixes.
Forked from [Krile StarryEyes](https://github.com/karno/StarryEyes).

## Target Platform

Windows 7 or later (we recommend Windows 8.1 x64), and .NET Framework 4.7 .

## Getting Started

Visit [krile.starwing.net](http://krile.starwing.net/) .

## License
This software is licensed under the MIT/X11 License.  
See LICENSE.TXT for more detail.

(C)2013 Karno.  
And other contributors have some rights of code fragments. see the commit log for more detail.
